function appRouting($locationProvider, $stateProvider) {

    $locationProvider.html5Mode(true).hashPrefix('!');

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: '/app/views/login.html',
            controller: loginCtrl,
            controllerAs: 'login'
        })
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: '/app/views/index.html',
            controller: indexCtrl,
            controllerAs: 'index'
        });
}
