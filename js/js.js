//= require config/dependencies.js
angular.module('innovoApp', ['ui.router'])
    .config(appRouting);

function appRouting($locationProvider, $stateProvider) {

    $locationProvider.html5Mode(true).hashPrefix('!');

    $stateProvider
        .state('index', {
            url: '/',
            templateUrl: '/app/views/index.html',
            controller: indexCtrl,
            controllerAs: 'index'
        })
        .state('login', {
            url: '/login',
            templateUrl: '/app/views/login.html',
            controller: loginCtrl,
            controllerAs: 'login'
        });
}

function indexCtrl() {
    var vm = this;

    console.log("indexCtrl Loaded");
}

function loginCtrl() {
    var vm = this;

    console.log("loginCtrl Loaded");
}
