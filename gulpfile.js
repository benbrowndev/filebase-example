/*========================================

	Gulp /w SCSS + PostCSS

	Prerequisites: npm, gulp, postcss

	Setup:
	- Configure the project options
	- Install the node modules with "npm install"
	- Run gulp and browser-sync with "gulp watch"

	Commands:
	gulp watch		Watch for file changes and perform the correct tasks
	gulp styles		Compiles scss and runs postcss processors
	gulp scripts 	Concats and minifys dev js into one file for prod

	Author : Ben Brown

========================================*/


// Dependencies
var gulp = require('gulp'),
	sass = require('gulp-sass'),
	postcss = require('gulp-postcss'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify');

// PostCSS Processors
var processors = [
	require('autoprefixer')({ browsers: ['last 2 versions', '> 5%', 'ie 6-8']  }), // Apply vendor prefixes
	require('lost')(), // Lost Grid - https://github.com/corysimmons/lost
	require('rucksack-css'), // Rucksack CSS Extenstions - https://simplaio.github.io/rucksack/
	require('cssnano')({ discardComments: { removeAll: true }}), // Minify CSS
	require('cssnext')() // Usable CSS4 features - http://cssnext.io/
];

// Error handling
function handleError (error) {
	console.log(error.toString());
	this.emit('end');
}

// Components
var components = [];
components.js = [
	'angular/angular.js',
	'angular-animate/angular-animate.js',
	'angular-ui-router/release/angular-ui-router.js'
];
components.css = [

];

// Watch for style/script changes and sync browser if turned on
gulp.task('watch', function() {
	gulp.watch('app/styles/*.scss', ['styles']);
    gulp.watch('app/scripts/**/*.js', ['scripts']);
    gulp.watch('app/components/**/*', ['components']);
});

// Build styles
gulp.task('styles', function() {
	var build = gulp.src('app/styles/styles.scss')
		.pipe(sass())
		.pipe(postcss(processors))
		.on('error', handleError)
		.pipe(gulp.dest('css'));

	return build;
});

// Build scripts
gulp.task('scripts', function() {
	return gulp.src('app/scripts/**/*.js')
		.pipe(concat('js.js'))
		// .pipe(uglify())
		.on('error', handleError)
		.pipe(gulp.dest('js'));
});

// Build components
gulp.task('components', ['components-js', 'components-css']);

gulp.task('components-js', function() {
    components.js.forEach(function(component, i) {
        components.js[i] = 'app/components/' + component;
    });

    return gulp.src(components.js)
        .pipe(concat('components.js'))
        .pipe(uglify())
        .on('error', handleError)
        .pipe(gulp.dest('js'));
});

gulp.task('components-css', function() {
    components.css.forEach(function(component, i) {
        components.css[i] = 'app/components/' + component;
    });

    return gulp.src(components.css)
        .pipe(concat('components.css'))
        .pipe(gulp.dest('css'));
});
